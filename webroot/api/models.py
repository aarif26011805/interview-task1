from django.db import models

# Create your models here.

class Organizations(models.Model):
    Company_Name = models.CharField(max_length=100)
    Email_ID = models.EmailField(max_length=100)
    Contact_Number = models.IntegerField()
    Subscription_Model  = models.CharField(max_length=100)
    License_Key = models.DateField(auto_created=True,auto_now_add=True)
