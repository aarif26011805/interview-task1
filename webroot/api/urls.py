from unicodedata import name
from django.contrib import admin
from django.urls import path,include
from .views import *

urlpatterns = [
    # path('admin/', admin.site.urls),
    # path('',include('api.urls'))
    path('create-org/',CreateOrg.as_view(),name="create organization"),
    path('read-org/',GetOrg.as_view(),name='read organizations'),
    path('read-orgid/<int:id>/',GetOrgID.as_view()),
    path('update-org/<int:id>/',UpdateOrg.as_view(),name="update org"),
    path('Delete-org/<int:id>/',DeleteOrg.as_view(),name='delete organization')


]
