from telnetlib import RSP
from tkinter.messagebox import NO
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import *
from .serializers import *


# Create your views here.

class CreateOrg(APIView):
    def post(self,request):
       
        serializer = OrganizationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"status":1,"message":"organizaion created successfully","data":serializer.data})
        return Response({"status":0,"message":serializer.errors})
class GetOrg(APIView):
    def get(self,request):
        task = Organizations.objects.all()
        serializer = OrganizationSerializer(task,many=True)
        return Response({"status":1,"data":serializer.data})
class GetOrgID(APIView):
    def get(self,request, id=None):
            task1 = Organizations.objects.get(id = id)
            serializer =OrganizationSerializer(task1)   
            return Response(serializer.data)

class UpdateOrg(APIView):
    def put(self,request,id=None):
        task = Organizations.objects.get(id=id)
        serializer = OrganizationSerializer(instance=task, data=request.data)
        if serializer.is_valid():
         serializer.save()
        return Response(
            {"message":"user updated successfully","data":serializer.data}
        )
class DeleteOrg(APIView):
    def delete(self,request,id=None):
        try:
            task = Organizations.objects.get(id = id)
            task.delete()
            return Response({"status":1,"message":"organization deleted successfully"})
        except Exception:
            return Response({"status":0,"message":"entrer the valid id"})
